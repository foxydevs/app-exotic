import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DireccionPage } from './direccion.page';
import { ModalDireccionComponent } from './modal-direccion/modal-direccion.component';

const routes: Routes = [
  {
    path: '',
    component: DireccionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DireccionPage,
    ModalDireccionComponent
  ], entryComponents: [
    ModalDireccionComponent
  ]
})
export class DireccionPageModule {}
