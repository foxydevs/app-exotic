import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/_service/producto.service';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-producto',
  templateUrl: './modal-producto.component.html',
  styleUrls: ['./modal-producto.component.scss'],
})
export class ModalProductoComponent implements OnInit {
  parameter:any;
  data:any;

  constructor(
    private productoService: ProductoService,
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  closeModal() {
    this.modalController.dismiss();
  }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingle(this.parameter)
  }

  getSingle(id:number) {
    this.productoService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data = res;
    }, (error) => {
      console.log(error)
    })
  }

}
